#-------------------------------------------------
#
# Project created by QtCreator 2015-06-25T12:14:58
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = FreeFormMotionProc
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

HEADERS += ../src/FreeFormMotionProc.h \
    ../src/utils_q.h

SOURCES += main.cpp \
    ../src/FreeFormMotionProc.cpp

LIBS += -fopenmp


QMAKE_CXXFLAGS += -fopenmp

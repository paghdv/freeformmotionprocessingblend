**Shape Preserving Frame Blending**
Implementation of the Frame Blending technique described in: Kircher and Garland, 2008, "Free-Form Motion Processing", TOG.
Main program provided.


Dependencies: Eigen3